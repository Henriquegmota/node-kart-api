const request = require('supertest');
const fs = require('mz/fs');
const app = require('./server.js');
const expect = require('expect');


let testFilePath = null;
describe('POST /file/upload - upload a new documentation file', () => {
  const filePath = `./test.txt`;

  // Upload first test file
  it('should upload the test file', () => 
    fs.exists(filePath)
      .then((exists) => {
        if (!exists) throw new Error('file does not exist'); 
        return request(app)
          .post('/file/upload')
           // adicionando arquivo no request
          .attach('file', filePath)
          .then((res) => {
            const { success, message, filePath } = res.body;
            expect(success);
            testFilePath = filePath;
          })
          .catch(err => console.log(err));
      })
  )})