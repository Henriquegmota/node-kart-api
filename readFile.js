const fs = require('fs');
const _ = require("underscore");
const prettyMs = require('pretty-ms');
const sortJsonArray = require('sort-json-array');
const glob = require('glob-fs')({ gitignore: true });
const path = require("path");


function isNotEmpty(value) {
  return value != "";
}
function voltasPiloto(value, id) {
  return (_.where(value, { codigoPiloto: id })).length;
}
function posicaoChegada(array, id) {
  var sort = sortJsonArray(array, 'tempoTotalProva', 'asc');
  var finalista = sortJsonArray(sort, 'qtdeVoltasCompletadas', 'des');
  var posicao = 0;
  for (i = 0; i < finalista.length; i++) {
    posicao = (finalista[i].codigoPiloto == id) ? i + 1 : posicao
  }
  return posicao
}

function velocidadeMedia(array, id) {
  var total = 0;
  for (i = 0; i < (_.where(array, { codigoPiloto: id })).length; i++) {  //loop through the array
    total += parseFloat(array[i].velocidadeMediaVolta);  //Do the math!
  }
  return total / i
}

function tempoCorrida(array, id) {
  var totalMili = 0;
  var arrayPiloto = _.where(array, { codigoPiloto: id });
  for (i = 0; i < arrayPiloto.length; i++) {  //loop through the array
    var tempo = arrayPiloto[i].tempoVolta.split(":");
    var minutos = parseInt(tempo[0]) * 60 * 1000;
    var segundos = parseInt(tempo[1].substr(0, 2)) * 1000;
    var millisegundos = parseInt(tempo[1].substr(3, 6));

    totalMili += minutos + segundos + millisegundos;

  }
  var tempoTotal = prettyMs(totalMili, { formatSubMs: true });

  var tmp = tempoTotal.split(" ");
  var min = (tmp[0].replace('m', '') < 10) ? "0" + tmp[0].replace('m', '') : tmp[0].replace('m', '');
  var seg = (tmp[1].replace('s', '') < 10) ? "0" + tmp[1].replace('s', '') : tmp[1].replace('s', '');
  var ms = (tmp[2].replace('ms', '') < 10) ? "0" + tmp[2].replace('ms', '') : tmp[2].replace('ms', '');
  return min + ":" + seg + "." + ms
}

function tempoAposVencedor(array, id) {
  var tempoVencedor = array[0].tempoTotalProva;
  var arrayPiloto = _.where(array, { codigoPiloto: id });
  var tempoPiloto = arrayPiloto[0].tempoTotalProva;

  //transformando em milisegundos Vencedor
  var tmpVencedor = tempoVencedor.split(":");
  var minutosVencedor = parseInt(tmpVencedor[0]) * 60 * 1000;
  var segundosVencedor = parseInt(tmpVencedor[1].substr(0, 2)) * 1000;
  var milisegundosVencedor = parseInt(tmpVencedor[1].substr(3, 6));

  var totalMiliVencedor = minutosVencedor + segundosVencedor + milisegundosVencedor;

  //transformando em milisegundos Piloto
  var tmpPiloto = tempoPiloto.split(":");
  var minutosPiloto = parseInt(tmpPiloto[0]) * 60 * 1000;
  var segundosPiloto = parseInt(tmpPiloto[1].substr(0, 2)) * 1000;
  var milisegundosPiloto = parseInt(tmpPiloto[1].substr(3, 6));

  var totalMiliPiloto = minutosPiloto + segundosPiloto + milisegundosPiloto;


  var diferençaTempo = totalMiliPiloto - totalMiliVencedor;


  var tempoTotal = prettyMs(diferençaTempo, { formatSubMs: true });


  if (arrayPiloto[0].qtdeVoltasCompletadas == 4) {
    if (diferençaTempo >= 60000) {
      var tmp = tempoTotal.split(" ");
      var min = (tmp[0].replace('m', '') < 10) ? "0" + tmp[0].replace('m', '') : tmp[0].replace('m', '');
      var seg = (tmp[1].replace('s', '') < 10) ? "0" + tmp[1].replace('s', '') : tmp[1].replace('s', '');
      var ms = (tmp[2].replace('ms', '') < 10) ? "0" + tmp[2].replace('ms', '') : tmp[2].replace('ms', '');
      return min + ":" + seg + "." + ms
    } else if (diferençaTempo >= 1000) {
      var tmp = tempoTotal.split(" ");
      var seg = (tmp[0].replace('s', '') < 10) ? "0" + tmp[0].replace('s', '') : tmp[0].replace('s', '');
      var ms = (tmp[1].replace('ms', '') < 10) ? "0" + tmp[1].replace('ms', '') : tmp[1].replace('ms', '');
      return "00:" + seg + "." + ms
    } else {
      return "00:00." + tempoTotal.replace('ms', '')
    }
  } else {
    return null
  }

}

function melhorVoltaPiloto(array, id) {
  var voltas = _.where(array, { codigoPiloto: id });
  var sort = sortJsonArray(voltas, 'tempoVolta', 'asc');


  return sort[0].numeroVolta
}

function melhorVoltaCorrida(array){
  var sort = sortJsonArray(array, 'tempoVolta', 'asc');

  var melhor = {
    'hora': sort[0].hora,
    'codigoPiloto': sort[0].codigoPiloto,
    'nomePiloto': sort[0].piloto,
    'numeroVolta': sort[0].numeroVolta,
    'tempoVolta': sort[0].tempoVolta,
    'velocidadeMediaVolta': sort[0].velocidadeMediaVolta
  }
  return melhor
}

function readFile() {
  glob.readdir('./uploads/*.txt', function (err, files) {
    console.log(files);

    var file = String(files[files.length - 1]);


    fs.readFile(file, (err, data) => {
      if (err) {
        console.error(err)
        return
      }

      var kart = [];
      console.log("arquivo encontrado")
      console.log(data.toString())
      //transformando arquivo em string
      var StringFile = data.toString();
      //fazendo a quebra da string conforme caracteres
      StringFile.split("\n").map(function (val) {
        var obj = val.split(" ").filter(isNotEmpty);
        var obj2 = obj.toString().split("\t").filter(isNotEmpty);
        var obj3 = obj2.toString().split(",");

        //criando array no formato com dados do arquivo
        kart.push({
          "hora": obj3[0],
          "codigoPiloto": obj3[1],
          "piloto": obj3[3],
          "numeroVolta": (obj3[4] == '') ? obj3[5] : obj3[4],
          "tempoVolta": (obj3[4] == '') ? obj3[6] : obj3[5],
          "velocidadeMediaVolta": (obj3[4] == '') ? (obj3[7] + '.' + obj3[8]).replace('\r', '') : (obj3[6] + '.' + obj3[7]).replace('\r', '')
        })

      });



      //removendo ultima linha caso estaja como undefined
      if (kart[kart.length - 1].piloto == undefined) {
        kart.pop(kart[kart.length - 1]);
      }
      //removendo primeira linha
      kart.shift();


      var corrida = [];
      kart.map(function (val) {
        corrida.push({
          "posicaoChegada": "",
          "codigoPiloto": val.codigoPiloto,
          "nomePiloto": val.piloto,
          "qtdeVoltasCompletadas": voltasPiloto(kart, val.codigoPiloto),
          "tempoTotalProva": tempoCorrida(kart, val.codigoPiloto),
          "melhorVolta": melhorVoltaPiloto(kart, val.codigoPiloto),
          "velocidadeMediaCorrida": velocidadeMedia(kart, val.codigoPiloto).toFixed(3),
          "tempoChegadaAposGanhador": ""
        })
      })
      var uniques = _.map(_.groupBy(corrida, function (doc) {
        return doc.codigoPiloto;
      }), function (grouped) {
        return grouped[0];
      });
      //response
      var voltas = [];
      uniques.map(function (val) {
        voltas.push({
          "posicaoChegada": posicaoChegada(uniques, val.codigoPiloto),
          "codigoPiloto": val.codigoPiloto,
          "nomePiloto": val.nomePiloto,
          "qtdeVoltasCompletadas": val.qtdeVoltasCompletadas,
          "tempoTotalProva": val.tempoTotalProva,
          "melhorVolta": val.melhorVolta,
          "velocidadeMediaCorrida": val.velocidadeMediaCorrida,
          "tempoChegadaAposGanhador": tempoAposVencedor(uniques, val.codigoPiloto)
        })
      })
      var response = {
        'melhorVoltaCorrida': melhorVoltaCorrida(kart),
        "voltas": voltas
      }
      console.log("response----------------")
      console.log(response)

      var pathfileName = path.basename(files.toString());
      var fileName = pathfileName.split(".");

      fs.writeFileSync("./output/" + fileName[0] + ".json", JSON.stringify(response));
      console.log("arquivo criado")

      fs.rename("./uploads/" + pathfileName, './log/' + pathfileName, function (err) {
        if (err) throw err;
        console.log('Move complete.');
      });

      return 1;

      //process.exit();
    });
  });
}



module.exports = {
  readFile: readFile
};