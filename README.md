# node-kart-api

# Rodando a aplicação

- abrir o terminal na pasta do projeto e executar o comando **node server.js**
- Após o serviço estar de pé, entrar na web e digitar **http://localhost:3000**
- clicar em escolher arquivo, e selecionar o arquivo de log.
- Após selecionar o arquivo, clicar em **upload!**
- Após você conseguirá ver pelo log o resultado do arquivo e também terá um arquivo com mesmo nome porém no formato **.json** localizado na pasta **output** e o arquivo original estará na pasta **log**


# Rodando o teste 

- abrir o terminal na pasta do projeto e executar o comando **npm test**
- No log você verá o resultado do arquivo de test utilizado, e também irá verificar **1 passing**

